import { createElement, addClass, removeClass } from './helper.mjs';

const username = sessionStorage.getItem('username');
let activeRoom = null;
let text = null;
let textArray = null;
let currentLetter = 0;
let winnersArray = [];
let inGame = false;

const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');
const roomsList = document.getElementById('rooms-list');
const addRoomBtn = document.getElementById('add-room-btn');
const infoBlock = document.getElementById('info-block');
const readyButton = document.getElementById('ready-btn');
const timer = document.getElementById('timer');
const textContainer = document.getElementById('text-container');
const gameTimer = document.getElementById('game-timer');
const quitGameBtn = document.getElementById('quit-results-btn');
const winners = document.getElementById('winners');
const winnersContainer = document.getElementById('winners-container');

const setActiveRoomId = (room) => {
  activeRoom = room;
  if (activeRoom) {
    addClass(roomsPage, 'display-none');
    removeClass(gamePage, 'display-none');
    renderGamePage(room);
  } else {
    removeClass(roomsPage, 'display-none');
    addClass(gamePage, 'display-none');
  }
};

if (!username) {
  window.location.replace('/login');
}

const socket = io('ws://localhost:3002', { query: { username } });

const renderGamePage = (room) => {
  if (!inGame) {
    const roomName = document.createElement('h1', '', { id: 'room-name' });
    roomName.innerText = room.id;
    const quitButton = createQuitButton(room.id);
    const userContainer = createElement('div', '', { id: 'users-container' });
    const userItems = room.users.map((el) => createUserDiv(el));
    userItems.forEach((item) => userContainer.appendChild(item));
    infoBlock.innerHTML = '';
    infoBlock.appendChild(roomName);
    infoBlock.appendChild(quitButton);
    infoBlock.appendChild(userContainer);
  } else {
    const deletedUsers = Array.prototype.slice
      .call(document.getElementById('users-container').children)
      .filter((item) =>
        hasNotUser(activeRoom.users, item.getAttribute('id').substr(10))
      )
      .map((el) => el.getAttribute('id').substr(10));
    deletedUsers.forEach((el) => {
      document
        .getElementById('users-container')
        .removeChild(document.getElementById(`users-div-${el}`));
      const index = winnersArray.indexOf(el);
      if (index !== -1) winnersArray.splice(index, 1);
    });
  }
};

const hasNotUser = (users, username) => {
  return users.every((item) => item.username !== username);
};

const updateRooms = (rooms) => {
  roomsList.innerHTML = '';
  const filteredRooms = rooms.filter((item) => item.isActive === true);
  const roomItems = filteredRooms.map((el) =>
    createRoomItem(el.id, el.users.length)
  );
  roomItems.forEach((item) => roomsList.appendChild(item));
};

const roomExist = () => {
  alert('Room already exists');
};

const userExist = () => {
  alert(
    'User with such username already exists. Please enter another username'
  );
  sessionStorage.removeItem('username');
  window.location.replace('/login');
};

const loadCurrentRoom = (room) => {
  setActiveRoomId(room);
};

const prepareTimer = (id) => {
  const quitButton = document.getElementById('quit-room-btn');
  addClass(quitButton, 'display-none');

  fetch(`/game/texts/${id}`)
    .then((res) => res.text())
    .then((data) => (text = data));

  showTimer();
  socket.emit('START_TIMER', activeRoom);
};

const startGame = () => {
  inGame = true;
  showGameWindow();
  document.addEventListener('keydown', onKeyPress);
  textArray = transformTextForGame(text);
  currentLetter = 0;
  if (textArray[currentLetter]) addClass(textArray[currentLetter], 'next');
  updateTextInGame(textArray);
  socket.emit('START_GAME_TIMER', activeRoom);
};

const updateTextInGame = (array) => {
  textContainer.innerText = '';
  array.forEach((item) => textContainer.appendChild(item));
};

const workTimer = (seconds) => {
  timer.innerText = seconds;
};

const workGameTimer = (seconds) => {
  gameTimer.innerText = seconds;
};

const showReadyBtn = () => {
  removeClass(readyButton, 'display-none');
  addClass(timer, 'display-none');
  addClass(textContainer, 'display-none');
  addClass(gameTimer, 'display-none');
  addClass(winnersContainer, 'display-none');
  addClass(quitGameBtn, 'display-none');
};

const showGameWindow = () => {
  addClass(readyButton, 'display-none');
  addClass(timer, 'display-none');
  removeClass(textContainer, 'display-none');
  removeClass(gameTimer, 'display-none');
  addClass(winnersContainer, 'display-none');
  addClass(quitGameBtn, 'display-none');
};

const showTimer = () => {
  addClass(readyButton, 'display-none');
  removeClass(timer, 'display-none');
  addClass(textContainer, 'display-none');
  addClass(gameTimer, 'display-none');
  addClass(winnersContainer, 'display-none');
  addClass(quitGameBtn, 'display-none');
};

const showModalWindow = () => {
  addClass(readyButton, 'display-none');
  addClass(timer, 'display-none');
  addClass(textContainer, 'display-none');
  addClass(gameTimer, 'display-none');
  removeClass(winnersContainer, 'display-none');
  removeClass(quitGameBtn, 'display-none');
};

const updateProgressBar = ({ username, percent }) => {
  const progressBar = document.getElementsByClassName(
    `user-progress ${username}`
  )[0];
  if (percent === 100) {
    addClass(progressBar, 'finished');
    winnersArray.push(username);
  }
  progressBar.style.width = `${percent}%`;
  isEveryOneIsDone();
};

const transformTextForGame = (text) => {
  const lettersArray = text.split('');
  const transformedArray = lettersArray.map((el) => createLetterSpan(el));
  return transformedArray;
};

const isEveryOneIsDone = () => {
  const progressBarArray = Array.prototype.slice.call(
    document.getElementsByClassName('user-progress')
  );
  if (progressBarArray.every((item) => item.classList.contains('finished')))
    socket.emit('EVERYONE_DONE', activeRoom);
};

const onClickAddBtn = () => {
  const roomName = window.prompt("Enter room's name", 'Name');
  if (roomName) {
    socket.emit('CREATE_ROOM', roomName);
  }
};

const onClickReadyBtn = () => {
  socket.emit('USER_READY', activeRoom);
  removeClass(readyButton, 'btn-success');
  addClass(readyButton, 'btn-danger');
  readyButton.removeEventListener('click', onClickReadyBtn);
  readyButton.addEventListener('click', onClickNotReadyBtn);
  readyButton.innerText = 'Not Ready';
};

const onClickNotReadyBtn = () => {
  socket.emit('USER_NOT_READY', activeRoom);
  removeClass(readyButton, 'btn-danger');
  addClass(readyButton, 'btn-success');
  readyButton.removeEventListener('click', onClickNotReadyBtn);
  readyButton.addEventListener('click', onClickReadyBtn);
  readyButton.innerText = 'Ready';
};

const onKeyPress = (event) => {
  const letter = event.key;
  if (textArray[currentLetter]) {
    if (textArray[currentLetter].textContent === letter) {
      addClass(textArray[currentLetter], 'done');
      removeClass(textArray[currentLetter], 'next');
      if (textArray[currentLetter + 1])
        addClass(textArray[currentLetter + 1], 'next');
      let progress;
      if (textArray && textArray.length !== 0) {
        progress = (currentLetter / (textArray.length - 1)) * 100;
      }
      socket.emit('PROGRESS_BAR_CHANGED', {
        room: activeRoom,
        username,
        percent: progress,
      });
      currentLetter++;
    }
  }
};

const finishGame = () => {
  inGame = false;
  stopTimer();
  showModalWindow();
  quitGameBtn.addEventListener('click', restartGame);
  document.removeEventListener('keydown', onKeyPress);
  const winnerItemsArray = winnersArray.map((item, index) =>
    createWinnerDiv(item, index + 1)
  );
  winners.innerHTML = '';
  winnerItemsArray.forEach((item) => winners.appendChild(item));
};

const restartGame = () => {
  quitGameBtn.removeEventListener('click', restartGame);
  onClickNotReadyBtn();
  showReadyBtn();
  text = null;
  textArray = null;
  currentLetter = 0;
  winnersArray = [];
  socket.emit('RESET_GAME', activeRoom);
};

const stopTimer = () => {
  socket.emit('STOP_TIMER');
};

socket.on('USER_EXIST', userExist);
socket.on('ROOM_EXIST', roomExist);
socket.on('UPDATE_ROOMS', updateRooms);
socket.on('JOIN_ROOM_DONE', loadCurrentRoom);
socket.on('QUIT_DONE', loadCurrentRoom);
socket.on('EVERY_USER_READY', prepareTimer);
socket.on('START_GAME', startGame);
socket.on('TIMER_BEFORE', workTimer);
socket.on('GAME_TIMER', workGameTimer);
socket.on('UPDATE_PROGRESS_BAR_FRONT', updateProgressBar);
socket.on('FINISH_GAME', finishGame);

addRoomBtn.addEventListener('click', onClickAddBtn);
readyButton.addEventListener('click', onClickReadyBtn);

// Additional function
const createWinnerDiv = (winner, position) => {
  const element = createElement('div', `place-${position}`);
  element.innerText = `${position}.` + winner;
  return element;
};

const createLetterSpan = (letter) => {
  const element = createElement('span', '');
  element.innerText = letter;
  return element;
};

const createUserDiv = (user) => {
  const container = createElement('div', 'row progress-row', {
    id: `users-div-${user.username}`,
  });

  const infoWrapper = createElement('div', 'info-wrapper');
  const statusColor =
    user.isReady === true ? 'ready-status-green' : 'ready-status-red';
  const statusDiv = createElement('div', statusColor);
  const userName = createElement('strong', 'username');
  username === user.username
    ? (userName.innerText = user.username + ' (you)')
    : (userName.innerText = user.username);

  const progressDiv = createElement('div', 'progress');
  const progressBarDiv = createElement(
    'div',
    `user-progress ${user.username}`,
    {
      style: 'width:0%;',
    }
  );

  infoWrapper.appendChild(statusDiv);
  infoWrapper.appendChild(userName);
  progressDiv.appendChild(progressBarDiv);
  container.appendChild(infoWrapper);
  container.appendChild(progressDiv);

  return container;
};

const createRoomButton = (roomId) => {
  const roomButton = createElement('button', 'btn btn-primary', {
    type: 'button',
    id: roomId,
  });

  const onJoinRoom = () => {
    socket.emit('JOIN_ROOM', roomId);
  };

  roomButton.addEventListener('click', onJoinRoom);

  roomButton.innerText = 'Join';

  return roomButton;
};

const createQuitButton = (roomId) => {
  const quitButton = createElement('button', 'btn btn-primary', {
    type: 'button',
    id: 'quit-room-btn',
  });

  const onQuit = () => {
    socket.emit('QUIT', roomId);
  };

  quitButton.addEventListener('click', onQuit);

  quitButton.innerText = 'Quit';

  return quitButton;
};

function createRoomItem(name, quantityOfUsers) {
  const roomDiv = createElement('div', 'room');

  const quanUsers = createElement('h6');
  let text;
  quantityOfUsers === 1
    ? (text = 'user connected')
    : (text = 'users connected');
  quanUsers.textContent = quantityOfUsers + ' ' + text;

  const roomName = createElement('h5');
  roomName.textContent = name;

  const btn = createRoomButton(name);

  roomDiv.appendChild(quanUsers);
  roomDiv.appendChild(roomName);
  roomDiv.appendChild(btn);

  return roomDiv;
}
