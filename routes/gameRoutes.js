import { Router } from 'express';
import path from 'path';
import { texts } from '../data';
import { HTML_FILES_PATH } from '../config';

const router = Router();

router
  .get('/', (req, res) => {
    const page = path.join(HTML_FILES_PATH, 'game.html');
    res.sendFile(page);
  })
  .get('/texts/:id', (req, res) => {
    const id = req.params.id;
    res.send(texts[id]);
  });

export default router;
