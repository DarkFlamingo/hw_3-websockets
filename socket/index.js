import * as config from './config';
import { texts } from '../data';

const users = [];
let rooms = [];
/* 
{
  id: string,
  users: User[],
  isActive: boolean
}*/
let timer = null;

const isExistingRoom = (rooms, id) => {
  const index = rooms.findIndex((room) => room.id === id);
  if (index !== -1) {
    return true;
  }
  return false;
};

const isEveryUserReady = (users) => {
  return users.every((el) => el.isReady === true);
};

const isEmptyRoom = (roomName) => {
  return !rooms.every(
    (room) => room.id !== roomName || room.users.length !== 0
  );
};

const deleteRoom = (roomName) => {
  rooms = rooms.filter((room) => room.id !== roomName);
};

const getRoomById = (id) => {
  const index = rooms.findIndex((room) => room.id === id);
  if (index !== -1) {
    return rooms[index];
  }
  return null;
};

const getRoomByUsername = (username) => {
  for (let i = 0; i < rooms.length; i++) {
    for (let j = 0; j < rooms[i].users.length; j++) {
      if (rooms[i].users[j].username === username) {
        return rooms[i];
      }
    }
  }
  return null;
};

const getRandomTextId = () => {
  if (texts.length) {
    const min = Math.ceil(0);
    const max = Math.floor(texts.length);
    return Math.floor(Math.random() * (max - min)) + min;
  } else {
    return null;
  }
};

const getCurrentRoomId = (socket) => {
  Object.keys(socket.rooms).find((room) => isExistingRoom(rooms, room));
};

const addUsers = (id, username) => {
  const active =
    getRoomById(id).users.length + 1 === config.MAXIMUM_USERS_FOR_ONE_ROOM
      ? false
      : true;
  rooms = rooms.map((room) =>
    room.id !== id
      ? room
      : {
          ...room,
          users: [...room.users, { username, isReady: false }],
          isActive: active,
        }
  );
};

const removeUsers = (id, username) => {
  rooms = rooms.map((room) =>
    room.id !== id
      ? room
      : {
          ...room,
          users: room.users.filter((user) => user.username !== username),
          isActive: true,
        }
  );
};

const startTimer = (timeSeconds, cb, cb_2) => {
  timer = setTimeout(() => {
    if (timeSeconds <= 0) {
      cb_2();
    } else {
      --timeSeconds;
      startTimer(timeSeconds, cb, cb_2);
      cb(timeSeconds);
    }
  }, 1000);
};

// const startTimer = (timeSeconds, cb, cb_) => {

// }

const changeRoomActive = (roomName, active) => {
  rooms = rooms.map((room) =>
    room.id !== roomName ? room : { ...room, isActive: active }
  );
};

const resetIsReady = (room) => {
  return {
    ...room,
    users: room.users.map((user) => ({ ...user, isReady: false })),
  };
};

export default (io) => {
  io.on('connection', (socket) => {
    const username = socket.handshake.query.username;

    if (users.findIndex((user) => user === username) === -1) {
      users.push(username);
    } else {
      socket.emit('USER_EXIST');
    }

    socket.emit('UPDATE_ROOMS', rooms);

    socket.on('disconnect', () => {
      const index = users.findIndex((user) => user === username);
      if (index !== -1) {
        const room = getRoomByUsername(username);
        let active = true;
        if (room) {
          active = getRoomById(room.id).isActive;
        }
        rooms.forEach((item) => removeUsers(item.id, username));
        users.splice(index, 1);
        if (room) {
          const updatedRoom = getRoomById(room.id);
          io.to(updatedRoom.id).emit(
            'JOIN_ROOM_DONE',
            getRoomById(updatedRoom.id)
          );
          if (
            isEveryUserReady(getRoomById(updatedRoom.id).users) &&
            active === true
          ) {
            changeRoomActive(updatedRoom.id, false);
            io.to(updatedRoom.id).emit('EVERY_USER_READY', getRandomTextId());
          }
          if (isEmptyRoom(updatedRoom.id)) {
            deleteRoom(updatedRoom.id);
          }
        }
        io.emit('UPDATE_ROOMS', rooms);
      }
    });

    socket.on('CREATE_ROOM', (roomName) => {
      if (getRoomById(roomName)) {
        socket.emit('ROOM_EXIST');
      } else {
        const prevRoomId = getCurrentRoomId(socket);
        if (roomName === prevRoomId) {
          return;
        }
        if (prevRoomId) {
          socket.leave(prevRoomId);
          removeUsers(prevRoomId, username);
        }
        socket.join(roomName, () => {
          rooms.push({
            id: roomName,
            isActive: true,
            users: [{ username: username, isReady: false }],
          });
          io.emit('UPDATE_ROOMS', rooms);
          socket.emit('JOIN_ROOM_DONE', getRoomById(roomName));
        });
      }
    });

    socket.on('JOIN_ROOM', (roomName) => {
      const room = getRoomById(roomName);
      if (room) {
        const prevRoomId = getCurrentRoomId(socket);
        if (roomName === prevRoomId) {
          return;
        }
        if (prevRoomId) {
          socket.leave(prevRoomId);
          removeUsers(prevRoomId, username);
        }
        socket.join(roomName, () => {
          addUsers(roomName, username);
          io.emit('UPDATE_ROOMS', rooms);
          io.to(roomName).emit('JOIN_ROOM_DONE', getRoomById(roomName));
        });
      }
    });

    socket.on('QUIT', (roomName) => {
      socket.leave(roomName);
      removeUsers(roomName, username);
      if (isEmptyRoom(roomName)) {
        deleteRoom(roomName);
      }
      io.emit('UPDATE_ROOMS', rooms);
      io.to(roomName).emit('JOIN_ROOM_DONE', getRoomById(roomName));
      socket.emit('QUIT_DONE', null);
    });

    socket.on('USER_READY', (room) => {
      rooms = rooms.map((item) =>
        item.id !== room.id
          ? item
          : {
              ...item,
              users: item.users.map((user) =>
                user.username !== username ? user : { ...user, isReady: true }
              ),
            }
      );
      io.to(room.id).emit('JOIN_ROOM_DONE', getRoomById(room.id));
      if (isEveryUserReady(getRoomById(room.id).users)) {
        changeRoomActive(room.id, false);
        io.to(room.id).emit('EVERY_USER_READY', getRandomTextId());
      }
      io.emit('UPDATE_ROOMS', rooms);
    });

    socket.on('USER_NOT_READY', (room) => {
      rooms = rooms.map((item) =>
        item.id !== room.id
          ? item
          : {
              ...item,
              users: item.users.map((user) =>
                user.username !== username ? user : { ...user, isReady: false }
              ),
            }
      );
      io.to(room.id).emit('JOIN_ROOM_DONE', getRoomById(room.id));
    });

    socket.on('START_TIMER', (room) => {
      startTimer(
        config.SECONDS_TIMER_BEFORE_START_GAME,
        (seconds) => {
          io.to(room.id).emit('TIMER_BEFORE', seconds);
        },
        () => {
          io.to(room.id).emit('START_GAME');
        }
      );
    });

    socket.on('START_GAME_TIMER', (room) => {
      startTimer(
        config.SECONDS_FOR_GAME,
        (seconds) => {
          io.to(room.id).emit('GAME_TIMER', seconds);
        },
        () => {
          socket.emit('FINISH_GAME');
        }
      );
    });

    socket.on('PROGRESS_BAR_CHANGED', ({ room, username, percent }) => {
      io.to(room.id).emit('UPDATE_PROGRESS_BAR_FRONT', { username, percent });
    });

    socket.on('EVERYONE_DONE', () => {
      socket.emit('FINISH_GAME');
    });

    socket.on('STOP_TIMER', () => {
      clearTimeout(timer);
    });

    socket.on('RESET_GAME', (room) => {
      rooms = rooms.map((el) => (el.id === room.id ? resetIsReady(el) : el));
      clearTimeout(timer);
      io.emit('UPDATE_ROOMS', rooms);
      io.to(room.id).emit('JOIN_ROOM_DONE', getRoomById(room.id));
    });
  });
};
